*Reflection: this was intended to be a script for convenience and for even the most basic user. Requiring them to get `wine` themselves kind of defeats that. Also, trying to get the .desktop file set up across all distributions is a nightmare.*
**This project is abandoned. Please see this article (article forthcoming) for a guide based off of this process, with easy commands to copy and paste on Ubuntu**

# Quran Flash Installer

See this article for a more detailed write-up [article forthcoming]

[QuranFlash](http://www.quranflash.com/home?en) is a brilliant desktop Qur'an reader with all the needed features. It runs flawlessly through Wine for Linux, and this script should make it easy to install it immediately. It will also make a .desktop file so you can run it easily from 

Just download this script to a folder, open the folder in a terminal (for example: `cd Downloads`), and then run `bash quranFlashInstaller.sh`. 

Note: **you must have WINE & `winbind` (both easily installable by `apt` in Ubuntu) installed already before running this**. 