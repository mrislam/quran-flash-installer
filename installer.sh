#!/bin/bash

# Setting up folders
mkdir ~/Downloads/QuranFlash
cd ~/Downloads/QuranFlash
echo Environment set-up: Success! 

# Downloading the installer & icon
wget "https://s3.amazonaws.com/quranflash/Quranflash.exe" 
wget "http://www.quranflash.com/images/logo230.png"
echo Download: Success!

read -p "Please note: the install will now begin. Please do not change any of the values from default, and do not launch application after install. Please be patient as install may take a few minutes. Press [Enter] now to start install"

# installing using wine
wine Quranflash.exe
echo Install: Success!

# Creating .desktop file
touch ~/.local/share/applications/Quran\ Flash.desktop
echo -n > ~/.local/share/applications/Quran\ Flash.desktop

echo "[Desktop Entry]" >> ~/.local/share/applications/Quran\ Flash.desktop
echo "Name=Quran Flash" >> ~/.local/share/applications/Quran\ Flash.desktop
echo "Type=Application" >> ~/.local/share/applications/Quran\ Flash.desktop
echo "Exec=sh -c \"cd ~/Downloads/QuranFlash/%SystemDrive%/Program\ Files\ \(x86\)/Quranflash\ Desktop; wine Quranflash\ Desktop.exe\"" >> ~/.local/share/applications/Quran\ Flash.desktop
echo "Comment=Desktop Quran reader" >> ~/.local/share/applications/Quran\ Flash.desktop
echo "Terminal=false" >> ~/.local/share/applications/Quran\ Flash.desktop
echo "Icon=~/Downloads/QuranFlash/logo230.png" >> ~/.local/share/applications/Quran\ Flash.desktop >> ~/.local/share/applications/Quran\ Flash.desktop

chmod +x ~/.local/share/applications/Quran\ Flash.desktop

echo Desktop: Success!
echo Note: You may need to restart before seeing the application in your menus/launcher
